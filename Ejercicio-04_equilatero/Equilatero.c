#include<stdio.h>

int main () {

    float fLado, fPerimetro;

    printf("Ingresa la medida del lado del triangulo: ");
    scanf("%f", &fLado);

    if(fLado <= 0){

        printf("La medida del lado no puede ser menor o igual a 0.");

    } else {

        fPerimetro = fLado * 3;
        printf("El perimetro del triangulo es de %.2f", fPerimetro);

    }

    fflush(stdin);
    getchar();
    return 0;

}