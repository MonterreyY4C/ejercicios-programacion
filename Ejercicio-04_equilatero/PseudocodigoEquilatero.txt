Inicio

    Variables fLado, fPerimetro : Punto Flotante

    Escribir ("Ingresa la medida del lado del triangulo: ")
    Leer fLado
    Si (fLado <= 0) entonces
        Escribir ("La medida del lado no puede ser menor o igual a 0.")
    Si no
        Hacer fPerimetro = (fLado * 3)
        Escribir ("El perimetro del triangulo es de ", fPerimetro)
    Fin si
    
Fin