Inicio

    Variables fLado1, fLado2, fLado3, fPerimetro : Punto Flotante

    Escribir ("Ingresa la medida del primer lado del triangulo: ")
    Leer fLado1
    Escribir ("Ingresa la medida del segundo lado del triangulo: ")
    Leer fLado2
    Escribir ("Ingresa la medida del tercer lado del triangulo: ")
    Leer fLado3
    Si (fLado1 <= 0 OR fLado2 <= 0 OR fLado3 <= 0) entonces
        Escribir ("La medida de un lado no puede ser menor o igual a 0.")
    Si no
        Hacer fPerimetro = fLado1 + fLado2 + fLado3
        Escribir ("El perimetro del triangulo es de ", fPerimetro)
    Fin si
    
Fin