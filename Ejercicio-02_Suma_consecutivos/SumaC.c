#include<stdio.h>

int main () {

    int eNumero = 0;
    int eSuma = 0;
    int eContador = 0;

    printf("Ingrese un numero entre el 1 y el 50: ");
    scanf("%d", &eNumero);

    if(eNumero > 0 && eNumero <= 50) {

        for( eContador = 0; eContador < eNumero; eContador++)
        {
            eSuma += eContador;
        }
        
        printf("La suma de los predecesores de %d es : %d", eNumero, eSuma);

    } else {

        printf("El numero ingresado no se encuentra entre el 1 y el 50.");

    }

    fflush(stdin);
    getchar();
    return 0;

}
